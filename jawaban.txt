NOMER 1 :
create database myshop;
use myshop;

NOMER 2 :
CREATE TABLE users(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE categories(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE items(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255),
    price int NOT NULL,
    stock int NOT NULL,
    category_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

NOMER 3 :
INSERT INTO users (name, email, password)
    VALUES ("John Doe", "john@doe.com", "john123");
INSERT INTO users (name, email, password)
    VALUES ("Jane Doe", "jane@doe.com", "jenita123");

INSERT INTO categories (name)
    VALUES ("gadget");
INSERT INTO categories (name)
    VALUES ("cloth");
INSERT INTO categories (name)
    VALUES ("men");
INSERT INTO categories (name)
    VALUES ("women");
INSERT INTO categories (name)
    VALUES ("branded");

INSERT INTO items (name, description, price, stock, category_id)
    VALUES ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", "1");
INSERT INTO items (name, description, price, stock, category_id)
    VALUES ("Uniklooh", "baju keren dari brand ternama", "500000", "50", "2");
INSERT INTO items (name, description, price, stock, category_id)
    VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000	", "10", "1");

NOMER 4 :
a.  SELECT * FROM users;
    SELECT id, name, email FROM users;

b.  SELECT * FROM items WHERE price > 1000000;
    SELECT * FROM items WHERE name LIKE 'uniklo%';

c.  SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name
    FROM items
    INNER JOIN categories ON items.id = categories.id;

NOMER 5 :
UPDATE items SET price = "2500000" WHERE name = "sumsang b50";